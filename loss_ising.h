/*
 * This file contains an functions for experiment done in paper
 * P. Richtárik and M. Takáč
 *      Parallel Coordinate Descent Methods for Big Data Optimization
 * http://www.optimization-online.org/DB_HTML/2012/11/3688.html
 */


/*
 * abstract_loss.h
 *
 *  Created on: 19 Jan 2012
 *      Author: jmarecek and taki
 */

#ifndef ISING_LOSS_H
#define ISING_LOSS_H

#include "loss_abstract.h"


struct ising_loss_traits: public loss_traits {
};

/*******************************************************************/
// a partial specialisation for logistic loss
template<typename L, typename D>
class Losses<L, D, ising_loss_traits> {

public:
	
// i = 0; i <p; i++
// j = i + 1; j < p; j++
// 此时theta范围是0~(p - 1)p/2 - 1个，根据i和j，theta对应的位置为 i*p - (i + 3)i/2 + j - 1
// 更新的列对应的数据分别是原始数据的第j和i列，res也只需要更新这两列对应的非零元
// 更新残差res[i * p ~ (i+1) * p - 1]和res[j * p ~ (j+1) * p -1]位置，分别用 
  static inline void recompute_residuals(const problem_data<L, D> &inst,
			std::vector<D> &residuals) {
    for (int i = 0; i < inst.m * inst.n; i++){    
      residuals[i] = 0;
    }
		for (int i = 0; i < inst.n - 1; i++) {
			for (int j = i + 1; j < inst.n; j++){
        L id = i * inst.n - (i + 3) * i/2 + j - 1;
        // 先计算j列元素对应的行的非零元的残差（i和j恰好会在数据中反过来,残差行对应的i列）
        for (unsigned int k = inst.A_csc_col_ptr[j]; k < inst.A_csc_col_ptr[j + 1]; k++){
          L id1 = i * inst.m + inst.A_csc_row_idx[k];
          residuals[id1] -= inst.x[id] * inst.b[id1] * inst.A_csc_values[k];
        }
        // 在计算i列元素对应的行的非零元的残差
        for (unsigned int l = inst.A_csc_col_ptr[i]; l < inst.A_csc_col_ptr[i + 1]; l++){
          L id2 = j * inst.m + inst.A_csc_row_idx[l];
          residuals[id2] -= inst.x[id] * inst.b[id2] * inst.A_csc_values[l];
        }
      }
		}
	}
	// 更新步长
	static inline D compute_update(const problem_data<L, D> &inst,
			const std::vector<D> &residuals, 
      const L &idx,  
      const std::vector<L> &row_col,
			const std::vector<D> &Li) {
		D partialDetivative = 0;
    for(unsigned int i = inst.A_csc_col_ptr[row_col[1]]; i < inst.A_csc_col_ptr[row_col[1] + 1]; i++){
        L row_id = row_col[2] * inst.m + inst.A_csc_row_idx[i];
        D tmp = exp(-residuals[row_id]);
        partialDetivative -= 1 / (1 + tmp) * inst.b[row_id] * inst.A_csc_values[i];
    }
    for(unsigned int j = inst.A_csc_col_ptr[row_col[2]]; j < inst.A_csc_col_ptr[row_col[2] + 1]; j++){
        L row_id1 = row_col[1] * inst.m + inst.A_csc_row_idx[j];
        D tmp = exp(-residuals[row_id1]);
        partialDetivative -= 1 / (1 + tmp) * inst.b[row_id1] * inst.A_csc_values[j];
    }
    return compute_soft_treshold(Li[idx] * inst.lambda, inst.x[idx] - Li[idx] * partialDetivative) - inst.x[idx];
  }

 // 序列更新残差res = tmp * A 和x = x + tmp，逐列操作更新各行残差，
 // 并行化更行
	static inline D do_single_iteration_parallel(const problem_data<L, D> &inst,
			const L &idx, 
      std::vector<L> &row_col, 
      std::vector<D> &residuals, 
      std::vector<D> &x,
			const std::vector<D> &Li) {
		D tmp = compute_update(inst, residuals, idx, row_col, Li); 
    if (tmp != 0) {
      x[idx] += tmp;
      //仅需更新两列变量对应的残差即可
      for (unsigned int i = inst.A_csc_col_ptr[row_col[1]]; i < inst.A_csc_col_ptr[row_col[1] + 1]; i++){
        L id1 = row_col[2] * inst.m + inst.A_csc_row_idx[i];
        parallel::atomic_add(residuals[id1], -tmp * inst.b[id1] * inst.A_csc_values[i]);
      } 
		  for (unsigned int j = inst.A_csc_col_ptr[row_col[2]]; j < inst.A_csc_col_ptr[row_col[2] + 1]; j++){
        L id2 = row_col[1] * inst.m + inst.A_csc_row_idx[j];
        parallel::atomic_add(residuals[id2], -tmp * inst.b[id2] * inst.A_csc_values[j]);
      } 
    }  
		return tmp;
	}
  // 计算目标函数 resids + lambda * sum(|x|)
	static inline D compute_fast_objective(const problem_data<L, D> &part,
			const std::vector<D> &residuals) {
		D resids = 0;
		D sumx = 0;
#pragma omp parallel for reduction(+:resids)		
    for (L i = 0; i < part.m * part.n; i++) {
			resids += log(1 + exp(residuals[i]));
		}
#pragma omp parallel for reduction(+:sumx)
		for (L j = 0; j < part.n * (part.n - 1)/2; j++) {
			sumx += abs(part.x[j]);
		}
		return resids + part.lambda * sumx;
	}
  // 计算lipschitz的倒数
	static inline void compute_reciprocal_lipschitz_constants(
			const problem_data<L, D> &inst, std::vector<D> &h_Li) {
		for (int i = 0; i < inst.n - 1; i++) {
			for (int j = i + 1; j < inst.n; j++){
        L id = i * inst.n - (i + 3) * i/2 + j - 1;
        h_Li[id] = 0;
        // 先计算j列元素对应的行的非零元的残差（i和j恰好会在数据中反过来,残差行对应的i列）
        for (unsigned int k = inst.A_csc_col_ptr[j]; k < inst.A_csc_col_ptr[j + 1]; k++){
          L id1 = i * inst.m + inst.A_csc_row_idx[k];
          D tmp = inst.b[id1] * inst.A_csc_values[k];
          h_Li[id] += tmp * tmp; 
        }
        for (unsigned int l = inst.A_csc_col_ptr[i]; l < inst.A_csc_col_ptr[i + 1]; l++){
          L id2 = j * inst.m + inst.A_csc_row_idx[l];
          D tmp = inst.b[id2] * inst.A_csc_values[l];
          h_Li[id] += tmp * tmp; 
        }
        if (h_Li[id] > 0) // Check is there should be "4" or not!!!
          h_Li[id] = 4 / (inst.sigma * h_Li[id]); // Compute reciprocal Lipschitz Constants
      }
    }
  }
  // 此处根据每行元素的非零元个数不同来计算Lipschitz常数
   static inline void compute_reciprocal_lipschitz_constants_weight(
			const problem_data<L, D> &inst, std::vector<D> &h_Li, int &tau) {
    for (int i = 0; i < inst.n - 1; i++) {
			for (int j = i + 1; j < inst.n; j++){
        L id = i * inst.n - (i + 3) * i/2 + j - 1;
        h_Li[id] = 0;
        // 先计算j列元素对应的行的非零元的残差（i和j恰好会在数据中反过来,残差行对应的i列）
        for (unsigned int k = inst.A_csc_col_ptr[j]; k < inst.A_csc_col_ptr[j + 1]; k++){
          L id1 = i * inst.m + inst.A_csc_row_idx[k];
          D tmp_sigma = 1 + (inst.omega_ind[id1] - 1) * (tau - 1)/(inst.n * (inst.n - 1)/2 - 1);
          D tmp = inst.b[id1] * inst.A_csc_values[k];
          h_Li[id] += tmp * tmp * tmp_sigma; 
        }
        for (unsigned int l = inst.A_csc_col_ptr[i]; l < inst.A_csc_col_ptr[i + 1]; l++){
          L id2 = j * inst.m + inst.A_csc_row_idx[l];
          D tmp_sigma = 1 + (inst.omega_ind[id2] - 1) * (tau - 1)/(inst.n * (inst.n - 1)/2 - 1);
          D tmp = inst.b[id2] * inst.A_csc_values[l];
          h_Li[id] += tmp * tmp * tmp_sigma;
        }
        if (h_Li[id] > 0) // Check is there should be "4" or not!!!
          h_Li[id] = 4 / ( h_Li[id]); // Compute reciprocal Lipschitz Constants
      }
    }
  }
  
  
};

#endif /* ISING_LOSS_H */
