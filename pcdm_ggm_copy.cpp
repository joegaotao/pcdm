#include <RcppEigen.h>
#include <RcppGSL.h>
// [[Rcpp::depends(RcppEigen)]]
// [[Rcpp::depends(RcppGSL)]]
#include "structures_gm.h"
#include "c_libs_headers.h"
#include "gsl_random_helper.h"
#include "losses.h"
using Eigen::MatrixXd; //double matrix
using Eigen::MatrixXi; // integer matrix
using Eigen::VectorXd;
using Eigen::ArrayXd;
using Eigen::ArrayXXd;

using Rcpp::wrap;
using Rcpp::as;
using Rcpp::List;
using Rcpp::IntegerVector;
using Rcpp::NumericVector;
using Rcpp::NumericMatrix;
using Rcpp::Environment;
using Rcpp::Function;
using Rcpp::Named;

typedef Eigen::Map<MatrixXd> MapMat; // 输入矩阵必须是double的
typedef Eigen::Map<MatrixXi> MapMati; // 输入矩阵必须是integer的
typedef Eigen::Map<VectorXd> MapVec;
typedef Eigen::Map<ArrayXd>  MapArray;
typedef Eigen::SparseVector<double> SpVec;
typedef Eigen::SparseMatrix<double> SpMat;
typedef std::vector<double> NormalVec;

// 根据抽样的idx, 判断行列index
void get_row_col(std::vector<int> &row_col, unsigned  int &idx, double n){
  int i;
  for(i = 0; i < n - 1; i++){
    if(2 * idx >= (2 * n - 1) * i - i * i & 2 * idx < (2 * n - 1) * (i + 1) - (i + 1) * (i + 1))
      break;
  }
  row_col[1] = i;
  row_col[2] = idx - ((2 * n - 3) * i - i * i)/2 + 1;
}


template<typename L, typename D>
void run_computation(problem_data<L, D> &inst, std::vector<D> &beta, std::vector<D> &h_Li,
    int omp_threads, L n, L m, int maxit, int blockReduction, std::vector<gsl_rng *>& rs,
		ofstream& experimentLogFile, const int MAXIMUM_THREADS) {
   
  omp_set_num_threads(omp_threads);
	init_random_seeds(rs);
  
  for(L i = 0; i < (n * (n - 1)/2); i++)
    inst.x[i] = 0;
  std::vector<D> residuals(m * n) ;
	Losses<L, D, ggm_copy_loss_traits>::recompute_residuals(inst, residuals);
	D fvalInit = Losses<L, D, ggm_copy_loss_traits>::compute_fast_objective(inst, residuals);
	double totalRunningTime = 0;
	double iterations = 0;
  double prec1 = 1e-5;
	L perPartIterations = n * (n - 1) /2 / blockReduction;
	double additional = perPartIterations / (0.0 + n * (n - 1)/2);
	D fval_old = fvalInit;
  D fval = fval_old;
// store initial objective value
	experimentLogFile << setprecision(16) << omp_threads << "," << n << "," << m
			<< "," << totalRunningTime << "," << iterations
			<< "," << fval_old << endl;
	//iterate 
  std::vector<int> row_col(2, 0); // store the indexs of row and col
  
  // 设置终止条件，给定迭代次数和精度
  double diff = 1;
  int totalIt = 0;
  while(diff > prec1 || totalIt < maxit){
   
	//for (int totalIt = 0; totalIt < maxit; totalIt++) {
		double startTime = gettime_();
#pragma omp parallel for
		for (L it = 0; it < perPartIterations; it++) {
			// one step of the algorithm
			unsigned int idx = gsl_rng_uniform_int(gsl_rng_r, n * (n - 1)/2);
      get_row_col(row_col, idx, n);
      Losses<L, D, ggm_copy_loss_traits>::do_single_iteration_parallel(
							inst, idx, row_col, residuals, inst.x, h_Li);
		}
		double endTime = gettime_();
		iterations += additional;
		totalRunningTime += endTime - startTime;
		// recompute residuals  - this step is not necessary but if accumulation of rounding errors occurs it is useful
		omp_set_num_threads(MAXIMUM_THREADS);
		if (totalIt % 3 == 0) {
			Losses<L, D, ggm_copy_loss_traits>::recompute_residuals(inst, residuals);
		}
		//计算目标函数差
    fval = Losses<L, D, ggm_copy_loss_traits>::compute_fast_objective(inst, residuals);
    diff = fval - fval_old;
    fval_old = fval;
    // 计算目标函数差
    totalIt++;

		int nnz = 0;
    double x_rmse = 0;
#pragma omp parallel for reduction(+:nnz)
		for (int i = 0; i < (n * (n - 1)/2); i++){
			if (inst.x[i] != 0)
				nnz++;
    }
// beta - x
#pragma omp parallel for reduction(+:x_rmse)
    for(int i = 0; i < (n * (n - 1)/2); i++){
      x_rmse += (inst.x[i] - beta[i]) * (inst.x[i] - beta[i]);
    }
    inst.x_rmse = sqrt(x_rmse)/(n * (n - 1)/2);
		omp_set_num_threads(omp_threads);	
    
    cout << omp_threads << "," << n * (n - 1)/2 << ","
				<< m  << "," << totalRunningTime << ","
				<< iterations << "," << fval << "," << nnz
				<< "," << inst.x_rmse << endl;
    
		experimentLogFile << setprecision(16) << omp_threads << "," 
        << n * (n - 1) /2 << "," << m  << "," 
        << totalRunningTime << ","
				<< iterations << "," << fval << "," << nnz
				<< ", " << inst.x_rmse << endl;
  //}
  }
}
// 计算原始矩阵方差，给diag_theta赋初值
void Var(const SpMat &datX, NormalVec &diag_theta){
  diag_theta.resize(datX.cols());
#pragma omp parallel for schedule(dynamic)
  for(int i = 0; i < datX.cols(); i++){
    double tmp = 0;
    double mean = 0;
    double s = 0;
    for (SpMat::InnerIterator it(datX, i); it; ++it){
      mean += it.value();
    }
    mean =  mean/datX.cols();
    for (SpMat::InnerIterator it(datX, i); it; ++it){
      tmp = it.value() - mean;
      s += tmp * tmp;
    }
    diag_theta[i] = s/datX.rows() + 1e-4;//  防止方差为0情况
  }

}

void init_data_gm(problem_data<int, double> &inst, SpMat &datX, double lambda){
  int m = datX.rows();
  int n = datX.cols();
  // 初始化复合似然的方差，用各列方差代替
  Var(datX, inst.diag_theta);
  
  inst.n = n;
  inst.m = m;
  inst.lambda = lambda;
  inst.b.resize(m * n, 0);
  inst.A_csc_values.resize(datX.nonZeros(), 0);
  inst.A_csc_col_ptr.resize(n + 1, 0);
  inst.A_csc_row_idx.resize(datX.nonZeros(), 0);
  int iteration = 0;
  //每列按照方差加权X_j/sqrt(thet_j)
  for (int i = 0; i < datX.outerSize(); ++i){
    for (SpMat::InnerIterator it(datX, i); it; ++it){
      inst.A_csc_values[iteration] = it.value()/sqrt(inst.diag_theta[i]);
      //inst.A_csc_values[iteration] = it.value();
      inst.A_csc_row_idx[iteration] = it.row();
      iteration++;
    }
    inst.A_csc_col_ptr[i + 1] += iteration;
  }
    // 重新计算m * n各行的非零元素个数，实际上每次去掉每列横着数也可以
  for(int i = 0; i < n - 1; i++){
    for(int j = i + 1; j < n; j++){
      for(SpMat::InnerIterator it(datX, j); it; ++it){
        inst.b[i * m + it.row()]++;
      } 
      for(SpMat::InnerIterator iti(datX, i); iti; ++iti){
        inst.b[j * m + iti.row()]++;
      }
    }
  }

  for(int i = 0; i < m * n; i++){
    inst.omega_ind.push_back(inst.b[i]);
  }
  // 获取每行非零元个数的最大值
  int min = n * (n - 1)/2;
  double max = 0;
  for (long i = 0; i < m * n; i++) {
    if (inst.b[i] > max)
      max = inst.b[i];
    if (inst.b[i] < min)
      min = inst.b[i];
  }
  inst.omega = max;
  for(int i = 0; i < n; ++i){
    for(SpMat::InnerIterator it(datX, i); it; ++it){
      inst.b[i * m + it.row()] = it.value()/sqrt(inst.diag_theta[i]);
      //inst.b[i * m + it.row()] = it.value();
    }
  }
  inst.total_n = n * datX.nonZeros();
  inst.x.resize((n * (n - 1) / 2), 0);
  inst.x_rmse = 0;
}
/*
void compute_diag_theta(problem_data<int, double> &inst, SpMat &datX,  int n, int m){
  SpVec cache_theta(n); 
//#pragma omp parallel for
  for(int i = 0; i < n; i++){
#pragma omp parallel for
    for(int j = i + 1; j < n; j++){
      int id1 = i * n - (i + 3) * i/2 + j - 1;
      cache_theta.insert(j) = inst.x[id1]; 
    }
    if(i > 0){
      for(int k = i - 1; k >= 0; k--){
        cache_theta.insert(k) = inst.x[k * n - (k + 3) * k/2 + i - 1];
      }
    }
    double q = (datX * cache_theta).squaredNorm() / m;
    inst.diag_theta[i] = (-1 + sqrt(1 + 4 * q))/(2 * q);
    cache_theta.setZero();
  }
}
*/
// [[Rcpp::export]]
List pcdm_ggm(SEXP x_, SEXP beta_, SEXP lambda_, SEXP maxit_, SEXP weight_)
{
//BEGIN_RCPP
    problem_data<int, double> inst;
    // make the standard sparseMatrix format
    //SpMat datX(as<SpMat>(x_));
    MapMat XX(as<MapMat>(x_)); // x_必须是double类型，之前输入如果是0-1，需要矩阵*1.0转华夏
    SpMat datX = XX.sparseView();
    datX.makeCompressed();
    
    NormalVec beta(as<NormalVec>(beta_));
    int maxit(as<int>(maxit_));
    double lambda(as<double>(lambda_));
    int weight = int(as<bool>(weight_));
    // 初始化数据，包含了复合似然的方差的初始化
    init_data_gm(inst, datX, lambda);
   
    // 随机数初始化
    int m = datX.rows();
    int n = datX.cols();
 
    gsl_rng_env_setup();
    const gsl_rng_type * T;
  	gsl_rng * r;
  	T = gsl_rng_default;
  	r = gsl_rng_alloc(T);
    
    const int MAXIMUM_THREADS = 16;
	  std::vector<gsl_rng *> rs(MAXIMUM_THREADS);
	  for (int i = 0; i < MAXIMUM_THREADS; i++) {
		  rs[i] = gsl_rng_alloc(T);
		  gsl_rng_set(rs[i], i);
	  }
   init_omp_random_seeds();
   // 残差和v的初始化
   std::vector<double> h_Li(n * (n - 1)/2, 0);
   //std::vector<double> residuals(m, 0);
   //---------------------- Set output files
   ofstream histogramLogFile;
   histogramLogFile.open("results/large_scale_expeiment_ggm_histogram.log");
   ofstream experimentLogFile;
   experimentLogFile.open("results/large_scale_expeiment_ggm.log");
   
  // run_experiment<long, double>(inst, n, m, maxit, 1, rs, histogramLogFile,
   //         experimentLogFile, MAXIMUM_THREADS);
   omp_set_num_threads(MAXIMUM_THREADS);
   init_random_seeds(rs);
	 //-------------- set the number of threads which should be used.
  //SpVec cache_theta(n);
  int TH[1] = {1};
   for (int i = 0; i < 1; i++) {
     cout << "Running experiment with " << TH[i] << " threads" << endl;
     // 交叉迭代
     //double totalRunningTime;
    // for(int k = 0; k < maxit; k++){
     //   double startTime = gettime_();
       // (1) 计算精确矩阵的非对角元，PCDM算法，
       // 注意其中的loss计算都对X用方差进行了加权处理
        inst.sigma = 1 + (TH[i] - 1) * (inst.omega - 1) / (n * (n - 1)/2- 1);
        cout << setprecision(16) << "beta = " << inst.sigma << endl;
        
        if(weight < 1){
          Losses<int, double, ggm_copy_loss_traits>::compute_reciprocal_lipschitz_constants(inst, h_Li);
        }  
        else{
          Losses<int, double, ggm_copy_loss_traits>::compute_reciprocal_lipschitz_constants_weight(inst, h_Li, TH[i]);
        }
        cout << "begin" << endl;
        // 此处的计算注意终止条件，目标不变，参数收敛，需要给原来的run_computation设置终止条件 
        run_computation<int, double>(inst, beta, h_Li, TH[i], n, m, maxit, 1, rs, experimentLogFile, MAXIMUM_THREADS);
        // （2）计算复合似然的方差，即精确矩阵的对角元的倒数，此处主要计算一个|X\Theta_j|^2/m的值，然后根据公式可算方差
       // compute_diag_theta(inst, datX, n, m); 
        
        
      //  double endTime = gettime_();
      //  totalRunningTime += endTime - startTime; 
      //  cout << k << "," << totalRunningTime << endl;       
     //}
   }
   histogramLogFile.close();
   experimentLogFile.close(); 
   return List::create(Named("lambda") = lambda,
                       Named("beta") = inst.x,
                       Named("beta_rmse") = inst.x_rmse);
}



