/*
 * This file contains an functions for experiment done in paper
 * P. Richtárik and M. Takáč
 *      Parallel Coordinate Descent Methods for Big Data Optimization
 * http://www.optimization-online.org/DB_HTML/2012/11/3688.html
 */


/*
 * Commonly used libs in our project
 */

#ifndef C_LIBS_HEADERS_H_
#define C_LIBS_HEADERS_H_

//====================== Standard C Libs
#include <iostream>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <sstream>
# include <cmath>
#include <stddef.h>
#include <memory.h>
#include <iostream>
#include <iomanip>
using namespace std;
#include <vector>
#include "utils.h"

#endif /* C_LIBS_HEADERS_H_ */
