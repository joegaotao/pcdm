/*
 * This file contains an functions for experiment done in paper
 * P. Richtárik and M. Takáč
 *      Parallel Coordinate Descent Methods for Big Data Optimization
 * http://www.optimization-online.org/DB_HTML/2012/11/3688.html
 */

/*
 * abstract_loss.h
 *
 *  Created on: 19 Jan 2012
 *      Author: jmarecek and taki
 */

#ifndef ABSTRACT_LOSS_H
#define ABSTRACT_LOSS_H


#include "parallel_essentials.h"
#include "treshhold_functions.h"


template<typename L, typename D>
class problem_data;

// "Traits" defining what loss functions to use
struct loss_traits {
};

// An "abstract" template class
// We use only its partial specialisations
template<typename L, typename D, typename Traits>
class Losses {
public:
	// 按正常的列（行）来加和计算残差，(同)
	static inline void recompute_residuals(const problem_data<L, D> inst, std::vector<D> &residuals) {
	}
	// 序列更新残差和x，逐列（行）操作更新各项残差，(同)
	static inline D do_single_iteration_serial(const problem_data<L, D> inst, const L idx,
	std::vector<D> &residuals, std::vector<D> &x, const std::vector<D> Li) {
		D tmp = 0;
		return abs(tmp);
	}
	// 通过原子操作达到并行化，(同)
	static inline D do_single_iteration_parallel(const problem_data<L, D> inst, const L idx,
	std::vector<D> &residuals, std::vector<D> &x, const std::vector<D> Li) {
		D tmp = 0;
		return abs(tmp);
	}
	// 计算步长平方损失下的 更新步长tmp (不同)
	// 这一个函数是后续各个不同损失函数的关键不同之处
	static inline D compute_update(const problem_data<L, D> &inst, const std::vector<D> &residuals,
	const L idx, const std::vector<D> &Li) {
		D delta = 0;
		return delta;
	}
	// 计算目标函数，(不同)
	static inline D compute_fast_objective(const problem_data<L, D> &part, const std::vector<D> &residuals) {
		D resids = 0;
		return resids;
	}
	// 计算Lipschitz常数项（二次导）的倒数，并且是通过ESO获取的心的Lipschitz，(不同)
	// 这一个函数是后续各个不同损失的计算的不同之处之一
	static inline void compute_reciprocal_lipschitz_constants(const problem_data<L, D> &inst,
	std::vector<D> &h_Li) {
	}
  static inline void compute_reciprocal_lipschitz_constants_weight(const problem_data<L, D> &inst,
	std::vector<D> &h_Li, int &tau) {
	}
};

#endif /* ABSTRACT_LOSS_H */
