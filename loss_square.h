/*
 * This file contains an functions for experiment done in paper
 * P. Richtárik and M. Takáč
 *      Parallel Coordinate Descent Methods for Big Data Optimization
 * http://www.optimization-online.org/DB_HTML/2012/11/3688.html
 */


/*
 *  Created on: 19 Jan 2012
 *      Author: jmarecek and taki
 */

#ifndef SQUARE_LOSS_H
#define SQUARE_LOSS_H

#include "loss_abstract.h"

struct square_loss_traits: public loss_traits {
};

/*******************************************************************/
// a partial specialisation for square loss
template<typename L, typename D>
class Losses<L, D, square_loss_traits> {

public:

	// 按正常的列来加和计算残差，初始残差为-b，然后按csc格式逐列 -b+A_j * x_j
	static inline void recompute_residuals(const problem_data<L, D> &inst,
			std::vector<D> &residuals) {
    for (L i = 0; i < inst.m; i++) {
			residuals[i] = -inst.b[i];
		}
		for (L i = 0; i < inst.n; i++) {
			for (unsigned int j = inst.A_csc_col_ptr[i]; j < inst.A_csc_col_ptr[i + 1]; j++) {
        residuals[inst.A_csc_row_idx[j]] += inst.A_csc_values[j] * inst.x[i];
			}
		}
	}
// 计算步长平方损失下的 更新步长tmp，参照式子为2011年文章Iteration Complexity of Randomized Block-Coordinate Descent
	// 或者参见2012年Yuan的CDN文章附录B
	// 仍旧是逐列计算此处tmp显示残差值xy，Li是二次导乘了beta后成为的v参数
	static inline D compute_update(const problem_data<L, D> &inst,
			const std::vector<D> &residuals, const L idx,
			const std::vector<D> &Li) {
		D tmp = 0; //compute partial derivative f_idx'(x)
		for (unsigned int j = inst.A_csc_col_ptr[idx];
				j < inst.A_csc_col_ptr[idx + 1]; j++) {
			tmp += inst.A_csc_values[j] * residuals[inst.A_csc_row_idx[j]];
		}
		tmp = compute_soft_treshold(Li[idx] * inst.lambda, inst.x[idx] - Li[idx] * tmp) - inst.x[idx];
		return tmp;
	}
	// 序列更新残差res = tmp * A 和x = x + tmp，逐列操作更新各项残差，
	// 需要返回更新步长tmp么？
	// 通过原子操作达到并行化
	static inline D do_single_iteration_parallel(const problem_data<L, D> &inst,
			const L idx, std::vector<D> &residuals, std::vector<D> &x,
			const std::vector<D> &Li) {
		// D tmp = 0;
    D tmp = compute_update(inst, residuals, idx, Li);
		if(tmp != 0){
      parallel::atomic_add(x[idx], tmp);
      for (unsigned int j = inst.A_csc_col_ptr[idx]; j < inst.A_csc_col_ptr[idx + 1]; j++) {
        parallel::atomic_add(residuals[inst.A_csc_row_idx[j]], tmp * inst.A_csc_values[j]);
      }
		}
		return abs(tmp);
	}
	
	// 通过原子操作达到分布式是的并行化，此处与单纯并行化多出来一点东西residual_updates
	static inline D do_single_iteration_parallel_for_distributed(
			const problem_data<L, D> &inst, const L idx,
			std::vector<D> &residuals, std::vector<D> &x,
			const std::vector<D> &Li, D* residual_updates) {
		// D tmp = 0;
		D tmp = compute_update(inst, residuals, idx, Li);
    if(tmp != 0){
      x[idx] += tmp;
      for (unsigned int j = inst.A_csc_col_ptr[idx];j < inst.A_csc_col_ptr[idx + 1]; j++) {
        parallel::atomic_add(residual_updates[inst.A_csc_row_idx[j]], tmp * inst.A_csc_values[j]);
        }
    }
		return abs(tmp);
	}
	// 计算目标函数，0.5 residuals^2 + lambda * sum(|x|)
	static inline D compute_fast_objective(const problem_data<L, D> &part,
			const std::vector<D> &residuals) {
    D resids = 0;
		D sumx = 0;
//#pragma omp parallel for reduction(+:resids)   
    for (L i = 0; i < part.m; i++) {
      resids += residuals[i] * residuals[i];
    }
//#pragma omp parallel for reduction(+:sumx)
		for (L j = 0; j < part.n; j++) {
      sumx += abs(part.x[j]);
		}
		return 0.5 * resids + part.lambda * sumx;
	}
	// 计算Lipschitz常数项（二次导）的倒数，对于平方损失，二次倒就是|A_j|^2，因为抽样又乘上了一个sigma参数，是13年文章的beta
	// 是15年文章合起来的v参数
	static inline void compute_reciprocal_lipschitz_constants(
			const problem_data<L, D> &inst, std::vector<D> &h_Li) {
		for (unsigned int i = 0; i < inst.n; i++) {
			h_Li[i] = 0;
			for (unsigned int j = inst.A_csc_col_ptr[i];
					j < inst.A_csc_col_ptr[i + 1]; j++) {
        h_Li[i] +=  inst.A_csc_values[j] * inst.A_csc_values[j];
      } 
			if (h_Li[i] > 0)
				h_Li[i] = 1 / (inst.sigma * h_Li[i]); // Compute reciprocal Lipschitz Constants
		}
	}
  
  // 此处根据每行元素的非零元个数不同来计算Lipschitz常数
  static inline void compute_reciprocal_lipschitz_constants_weight(
      const problem_data<L, D> &inst, std::vector<D> &h_Li, int &tau) {
    double tmp_sigma;
    for (unsigned int i = 0; i < inst.n; i++){
      h_Li[i] = 0;
      for (unsigned int j = inst.A_csc_col_ptr[i];
          j < inst.A_csc_col_ptr[i + 1]; j++) {
        tmp_sigma = 1 + (inst.omega_ind[inst.A_csc_row_idx[j]] - 1) * (tau - 1)/(inst.n - 1);
        h_Li[i] += tmp_sigma *  inst.A_csc_values[j] * inst.A_csc_values[j];
      }
      if (h_Li[i] > 0)
        h_Li[i] = 1 /  h_Li[i]; // Compute reciprocal Lipschitz Constants
    }
  }

};

#endif /* SQUARE_LOSS_H */
