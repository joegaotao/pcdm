#include <RcppEigen.h>
#include <RcppGSL.h>
// [[Rcpp::depends(RcppEigen)]]
// [[Rcpp::depends(RcppGSL)]]
#include "structures.h"
#include "c_libs_headers.h"
#include "gsl_random_helper.h"
#include "losses.h"
using Eigen::MatrixXd;
using Eigen::VectorXd;
using Eigen::ArrayXd;
using Eigen::ArrayXXd;

using Rcpp::wrap;
using Rcpp::as;
using Rcpp::List;
using Rcpp::IntegerVector;
using Rcpp::NumericVector;
using Rcpp::NumericMatrix;
using Rcpp::Environment;
using Rcpp::Function;
using Rcpp::Named;

typedef Eigen::Map<MatrixXd> MapMat;
typedef Eigen::Map<VectorXd> MapVec;
typedef Eigen::Map<ArrayXd>  MapArray;
typedef Eigen::SparseVector<double> SpVec;
typedef Eigen::SparseMatrix<double> SpMat;
typedef std::vector<double> NormalVec;
typedef Eigen::ArrayXd ArrayXd;
typedef Eigen::MatrixXd MatrixXd;
typedef Eigen::VectorXd VectorXd;

template<typename L, typename D>
void run_computation(problem_data<L, D> &inst, std::vector<D> &beta, std::vector<D> &h_Li,
    std::vector<D> &residuals,
    int omp_threads, L n, L m, int maxit, int blockReduction, std::vector<gsl_rng *>& rs,
		ofstream& experimentLogFile, const int MAXIMUM_THREADS) {
  
  omp_set_num_threads(omp_threads);
	init_random_seeds(rs);
	for (L i = 0; i < n; i++)
		inst.x[i] = 0;
	//std::vector<D> residuals(m);
	Losses<L, D, square_loss_traits>::recompute_residuals(inst, residuals);
	D fvalInit = Losses<L, D, square_loss_traits>::compute_fast_objective(inst, residuals);
	double totalRunningTime = 0;
	double iterations = 0;
	L perPartIterations = n / blockReduction;
	double additional = perPartIterations / (0.0 + n);
	D fval = fvalInit;
// store initial objective value
	experimentLogFile << setprecision(16) << omp_threads << "," << n << "," << m
			<< "," << totalRunningTime << "," << iterations
			<< "," << fval << endl;
	//iterate
	for (int totalIt = 0; totalIt < maxit; totalIt++) {
		double startTime = gettime_();
#pragma omp parallel for
		for (L it = 0; it < perPartIterations; it++) {
			// one step of the algorithm
			unsigned long int idx = gsl_rng_uniform_int(gsl_rng_r, n);
      Losses<L, D, square_loss_traits>::do_single_iteration_parallel(
							inst, idx, residuals, inst.x, h_Li);
		}
		double endTime = gettime_();
		iterations += additional;
		totalRunningTime += endTime - startTime;
		// recompute residuals  - this step is not necessary but if accumulation of rounding errors occurs it is useful
		omp_set_num_threads(MAXIMUM_THREADS);
		if (totalIt % 3 == 0) {
			Losses<L, D, square_loss_traits>::recompute_residuals(inst, residuals);
		}
		fval = Losses<L, D, square_loss_traits>::compute_fast_objective(inst, residuals);
		int nnz = 0;
    double x_rmse = 0;
#pragma omp parallel for reduction(+:nnz)
		for (L i = 0; i < n; i++){
			if (inst.x[i] != 0)
				nnz++;
    }
// beta - x
#pragma omp parallel for reduction(+:x_rmse)
    for(int i = 0; i < n; i++){
      x_rmse += (inst.x[i] - beta[i]) * (inst.x[i] - beta[i]);
    }
    inst.x_rmse = sqrt(x_rmse)/n;
		omp_set_num_threads(omp_threads);
		cout << omp_threads << "," << n << ","
				<< m << "," << totalRunningTime << ","
				<< iterations << "," << fval << "," << nnz
				<< "," << inst.x_rmse << endl;

		experimentLogFile << setprecision(16) << omp_threads << "," << n << ","
				<< m << "," << totalRunningTime << ","
				<< iterations << "," << fval << "," << nnz
				<< ", " << inst.x_rmse << endl;
	}
  Losses<L, D, square_loss_traits>::recompute_residuals(inst, residuals);
}

// 用列的norm近似求解方差
void sdX(MatrixXd &XX, VectorXd &diag_theta, int m){
  diag_theta.noalias() = XX.colwise().norm()/sqrt(m);
}
/*
void transformX(MatrixXd &XX, SpMat &datX, VectorXd &diag_theta){ 
  datX = (XX * ((diag_theta.asDiagonal()).inverse())).sparseView();
  datX.makeCompressed();
}
*/
void update_diag_theta(NormalVec &residuals, VectorXd &diag_theta, int m, int n){
  diag_theta.setZero();
  for(int i = 0; i <  n; i++){
    for(int j = 0; j < m; j++){
     diag_theta[i] += residuals[i * m + j]; 
    }
    diag_theta[i] = sqrt(diag_theta[i]/m);
  }
}


void init_data(problem_data<int, double> &inst, 
    //SpMat &datX, 
    MatrixXd XX,
    VectorXd &diag_theta, int m, int n){
  //inst.b.resize(m * n, 0);
  //inst.A_csc_values
  //datX.makeCompressed();
  inst.A_csc_col_ptr.resize(1);
  inst.b.resize(m * n, 0);
  int iteration = 0;
  //inst.A_csc_row_idx.resize(datX.nonZeros(), 0);
  for (int i = 0; i < n - 1; i++){
    for (int j = i + 1; j < n; j++){
      //for (SpMat::InnerIterator it(datX, j); it; ++it){
      for(int k = 0; k < m; k++){
        iteration++;
        //inst.A_csc_row_idx.push_back(i * m + it.row());
        inst.A_csc_row_idx.push_back(i * m + k);
        //inst.A_csc_values.push_back(it.value() * diag_theta[i] * diag_theta[j]); // \tilde{X}*sqrt(sigma[i]*sigma[j])
        inst.A_csc_values.push_back(XX(k, j) * diag_theta[i]);
      }
      //for (SpMat::InnerIterator it(datX, i); it; ++it){
      for(int k = 0; k < m; k++){
        iteration++;
        //inst.A_csc_row_idx.push_back(j * m + it.row());
        inst.A_csc_row_idx.push_back(j * m + k);
        //inst.A_csc_values.push_back(it.value() * diag_theta[i] * diag_theta[j]);  // \tilde{X}*sqrt(sigma[i]*sigma[j])
        inst.A_csc_values.push_back(XX(k, i) * diag_theta[j]);
      }
      inst.A_csc_col_ptr.push_back(iteration);
    }
  }
  for (int i = 0; i < n; i++){
    //for (SpMat::InnerIterator it(datX, i); it; ++it){
    for(int k = 0; k < m; k++){
      //inst.b[i * m + it.row()] = -it.value(); // XB_j - (-X_j)
      inst.b[i * m + k] = -XX(k, i)/diag_theta[i];
    }
  }
  inst.omega = n - 1;
  // 获取每行非零元个数的最大值, 此处默认原始输入数据时稠密的
  inst.total_n =  n * m * n;
}

// [[Rcpp::export]]
List pcdm_lasso_ggm(SEXP x_, SEXP beta_, SEXP lambda_, SEXP maxit_, SEXP weight_)
{
//BEGIN_RCPP
    problem_data<int, double> inst;
    // make the standard sparseMatrix format
    //SpMat datX(as<SpMat>(x_));
    //MapMat X(as<MapMat>(x_));
    //MatrixXd XX = X;
    MatrixXd XX(as<MatrixXd>(x_));
    //SpMat datX;
    VectorXd diag_theta;

    NormalVec beta(as<NormalVec>(beta_));
    int maxit(as<int>(maxit_));
    double lambda(as<double>(lambda_));
    int weight = int(as<bool>(weight_));
    // 初始化数据 
    int m = XX.rows();
    int n = XX.cols();
    //cout << "m" << m << "n" << n << endl;
    inst.lambda = lambda;
    inst.m = m * n;
    inst.n = n * (n - 1)/2;
    inst.x.resize(inst.n, 0);
    // 初始化diag_theta，用标准差，因为后续都是用其开方值；
    sdX(XX, diag_theta, m);
    //cout << diag_theta << endl;
    // 变换数据初始化，给datX初始化
    //transformX(XX, datX, diag_theta);
    //cout << datX << endl;
    // inst数据初始化
    init_data(inst, XX, diag_theta, m, n);
    //cout << datX << endl;
    // 随机数初始化 
    gsl_rng_env_setup();
    const gsl_rng_type * T;
  	gsl_rng * r;
  	T = gsl_rng_default;
  	r = gsl_rng_alloc(T);
    
    const int MAXIMUM_THREADS = 16;
	  std::vector<gsl_rng *> rs(MAXIMUM_THREADS);
	  for (int i = 0; i < MAXIMUM_THREADS; i++) {
		  rs[i] = gsl_rng_alloc(T);
		  gsl_rng_set(rs[i], i);
	  }
   init_omp_random_seeds();
   // 残差和v的初始化
   std::vector<double> residuals(m * n, 0);
   std::vector<double> h_Li(n * (n - 1)/2, 0);
   //std::vector<double> residuals(m, 0);
   //---------------------- Set output files
   ofstream histogramLogFile;
   histogramLogFile.open("results/large_scale_expeiment_lasso_histogram.log");
   ofstream experimentLogFile;
   experimentLogFile.open("results/large_scale_expeiment_lasso.log");
   
  // run_experiment<long, double>(inst, n, m, maxit, 1, rs, histogramLogFile,
   //         experimentLogFile, MAXIMUM_THREADS);
   omp_set_num_threads(MAXIMUM_THREADS);
   init_random_seeds(rs);
	 //-------------- set the number of threads which should be used.
	 int TH[3] = {24, 4 };
   for (int i = 0; i < 4; i++) {
     cout << "Running experiment with " << TH[i] << " threads" << endl;
     inst.sigma = 1 + (TH[i] - 1) * (inst.omega - 1) / (inst.n - 1);
     cout << setprecision(16) << "beta = " << inst.sigma << endl;
     // for(int j = 0; j < maxit; j++){};
     // 变换数据
     //transformX(XX, datX, diag_theta);
     // 初始化inst数据
     // init_data(inst, datX, diag_theta, n, m);
     // 进入求解部分
     if(weight < 1){// 求Lipschitz常数
       Losses<int, double, square_loss_traits>::compute_reciprocal_lipschitz_constants(inst, h_Li);
     }
     else{
       Losses<int, double, square_loss_traits>::compute_reciprocal_lipschitz_constants_weight(inst, h_Li, TH[i]);
     }
     // run the experiment
   	 run_computation<int, double>(inst, beta, h_Li, residuals, TH[i], inst.n, inst.m, maxit, 1, rs, experimentLogFile, MAXIMUM_THREADS);
     // 根据所得残差更新diag_theta，循环求解,如果不怎么变化，就停止；
     // update_diag_theta(residuals, diag_theta);
   }
   
   histogramLogFile.close();
   experimentLogFile.close(); 
   
   return List::create(Named("b") = inst.b,
                       Named("x") = inst.A_csc_values,
                       Named("col_ptr") = inst.A_csc_col_ptr,
                       Named("row_idx") = inst.A_csc_row_idx,
                       Named("lambda") = lambda,
                       Named("beta") = inst.x,
                       Named("beta_rmse") = inst.x_rmse);
}



