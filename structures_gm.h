/*
 * This file contains an functions for experiment done in paper
 * P. Richtárik and M. Takáč
 *      Parallel Coordinate Descent Methods for Big Data Optimization
 * http://www.optimization-online.org/DB_HTML/2012/11/3688.html
 */

/*
 * Optimization and output structures,
 *
 * optimization_setting structure is used to configure solvers, how much can solver run,
 */

#ifndef OPTIMIZATION_STRUCTURES_H_
#define OPTIMIZATION_STRUCTURES_H_

#include <ios>
#include <cstdlib>
#include <unistd.h>

#include "sys/types.h"
#include "sys/sysinfo.h"

#include <algorithm>
#include <iterator>
#include <iostream>
#include <fstream>
#include <string>

template<typename L, typename D>
class problem_data {
public:
	std::vector<D> A_csc_values;
	std::vector<L> A_csc_row_idx;
	std::vector<L> A_csc_col_ptr;

  std::vector<D> omega_ind;
	std::vector<D> b;
  std::vector<D> diag_theta;
#ifdef NEWATOMICS
	std::vector< atomic_float<D, L> > x;
#else
	std::vector<D> x;
#endif

	L m;
	L n;
	L total_n;
	D lambda;
	D omega;
	D sigma;
  D x_rmse;
};

template<typename L, typename D>
class problem_mc_data {
public:
	std::vector<D> A_coo_values;
	std::vector<L> A_coo_row_idx;
	std::vector<L> A_coo_col_idx;
	std::vector<short int> A_coo_operator; // 0 is equality  -1 or +1 is inequality constraint
	D mu;
	L m;
	L n;
	L rank;

	std::vector<D> R_mat;
	std::vector<D> L_mat;
};


#endif /* OPTIMIZATION_STRUCTURES_H_ */

