/*
 * This file contains an functions for experiment done in paper
 * P. Richtárik and M. Takáč
 *      Parallel Coordinate Descent Methods for Big Data Optimization
 * http://www.optimization-online.org/DB_HTML/2012/11/3688.html
 */


/*
 * gsl_random_helper.h
 *
 *  Created on: Nov 25, 2012
 *      Author: taki
 */

#ifndef GSL_RANDOM_HELPER_H_
#define GSL_RANDOM_HELPER_H_

#include "openmp_helper.h"

#include <gsl/gsl_rng.h>

const gsl_rng_type * gsl_rng_T;
gsl_rng * gsl_rng_r;
#pragma omp threadprivate (gsl_rng_r)
#pragma omp threadprivate (gsl_rng_T)

void init_random_seeds(std::vector<gsl_rng *>& rs) {
#pragma omp parallel
	{
		my_thread_id = omp_get_thread_num();
		gsl_rng_r = rs[my_thread_id];
	}
	TOTAL_THREADS = 1;
#pragma omp parallel
	{
		TOTAL_THREADS = omp_get_num_threads();
	}
	unsigned int seed[TOTAL_THREADS];
#pragma omp parallel
	{
		gsl_rng_set(gsl_rng_r, my_thread_id);
		if (omp_get_thread_num() == 0) {
			srand(1);
			for (unsigned int i = 0; i < TOTAL_THREADS; i++)
				seed[i] = (unsigned int) rand();
		}
	}
#pragma omp parallel
	{
		myseed = seed[omp_get_thread_num()];
	}
}

#endif /* GSL_RANDOM_HELPER_H_ */
