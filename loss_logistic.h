/*
 * This file contains an functions for experiment done in paper
 * P. Richtárik and M. Takáč
 *      Parallel Coordinate Descent Methods for Big Data Optimization
 * http://www.optimization-online.org/DB_HTML/2012/11/3688.html
 */


/*
 * abstract_loss.h
 *
 *  Created on: 19 Jan 2012
 *      Author: jmarecek and taki
 */

#ifndef LOGISTIC_LOSS_H
#define LOGISTIC_LOSS_H

#include "loss_abstract.h"


struct logistic_loss_traits: public loss_traits {
};

/*******************************************************************/
// a partial specialisation for logistic loss
template<typename L, typename D>
class Losses<L, D, logistic_loss_traits> {

public:
	static inline void recompute_residuals(const problem_data<L, D> &inst,
			std::vector<D> &residuals) {
    for(int i = 0; i < inst.m; i++){
      residuals[i] = 0;
    }
		for (L i = 0; i < inst.n; i++) {
			for (unsigned int j = inst.A_csc_col_ptr[i];
					j < inst.A_csc_col_ptr[i + 1]; j++) {
        residuals[inst.A_csc_row_idx[j]] -= inst.x[i] * inst.b[inst.A_csc_row_idx[j]] * inst.A_csc_values[j];
			}
		}
	}
	// 更新步长
	static inline D compute_update(const problem_data<L, D> &inst,
			const std::vector<D> &residuals, const L &idx,
			const std::vector<D> &Li) {
		D partialDetivative = 0;
		for (unsigned int j = inst.A_csc_col_ptr[idx];
				j < inst.A_csc_col_ptr[idx + 1]; j++) {
			L row_id = inst.A_csc_row_idx[j];
			D tmp = exp(-residuals[row_id]); //1/(1+exp( -res)); res = -yxb
			partialDetivative -= 1 / (1 + tmp) * inst.b[row_id]	* inst.A_csc_values[j];
		}
		return compute_soft_treshold(Li[idx] * inst.lambda, inst.x[idx] - Li[idx] * partialDetivative) - inst.x[idx];
	}

 // 序列更新残差res = tmp * A 和x = x + tmp，逐列操作更新各项残差，
 // 并行化更行
	static inline D do_single_iteration_parallel(const problem_data<L, D> &inst,
			const L &idx, std::vector<D> &residuals, std::vector<D> &x,
			const std::vector<D> &Li) {
		D tmp = compute_update(inst, residuals, idx, Li);
    if (tmp != 0) {
      parallel::atomic_add(x[idx], tmp);
		  for (unsigned int j = inst.A_csc_col_ptr[idx]; j < inst.A_csc_col_ptr[idx + 1]; j++) {
        parallel::atomic_add(residuals[inst.A_csc_row_idx[j]], 
          -tmp * inst.b[inst.A_csc_row_idx[j]] * inst.A_csc_values[j]);
      }
    }
		return tmp;
	}
  // 计算目标函数 resids + lambda * sum(|x|)
	static inline D compute_fast_objective(const problem_data<L, D> &part,
			const std::vector<D> &residuals) {
		D resids = 0;
		D sumx = 0;
		for (L i = 0; i < part.m; i++) {
			resids += log(1 + exp(residuals[i]));
		}
		for (L j = 0; j < part.n; j++) {
			sumx += abs(part.x[j]);
		}
		return resids + part.lambda * sumx;
	}
  // 计算lipschitz的倒数
	static inline void compute_reciprocal_lipschitz_constants(
			const problem_data<L, D> &inst, std::vector<D> &h_Li) {
    for (unsigned int i = 0; i < inst.n; i++) {
			h_Li[i] = 0;
			for (unsigned int j = inst.A_csc_col_ptr[i];
					j < inst.A_csc_col_ptr[i + 1]; j++) {
				D tmp = inst.A_csc_values[j] * inst.b[inst.A_csc_row_idx[j]];
        h_Li[i] +=  tmp * tmp;
			}
			if (h_Li[i] > 0) // Check is there should be "4" or not!!!
				h_Li[i] = 4 / (inst.sigma * h_Li[i]); // Compute reciprocal Lipschitz Constants
		}
	}
  // 此处根据每行元素的非零元个数不同来计算Lipschitz常数
  static inline void compute_reciprocal_lipschitz_constants_weight(
      const problem_data<L, D> &inst, std::vector<D> &h_Li, int &tau) {
    for (unsigned int i = 0; i < inst.n; i++){
      h_Li[i] = 0;
      for (unsigned int j = inst.A_csc_col_ptr[i];
          j < inst.A_csc_col_ptr[i + 1]; j++) {
        D tmp_sigma = 1 + (inst.omega_ind[inst.A_csc_row_idx[j]] - 1) * (tau - 1)/(inst.n - 1);
        D tmp = inst.A_csc_values[j] * inst.b[inst.A_csc_row_idx[j]];
        //parallel::atomic_add(h_Li[i], tmp_sigma *  tmp * tmp);
        h_Li[i] += tmp_sigma * tmp * tmp;
      }
      if (h_Li[i] > 0)
        h_Li[i] = 4 /  h_Li[i]; // Compute reciprocal Lipschitz Constants
    }
  }


};

#endif /* LOGISTIC_LOSS_H */
