/*
 * This file contains an functions for experiment done in paper
 * P. Richtárik and M. Takáč
 *      Parallel Coordinate Descent Methods for Big Data Optimization
 * http://www.optimization-online.org/DB_HTML/2012/11/3688.html
 */


/*
 * openmp_helper.h
 *
 *  Created on: Nov 25, 2012
 *      Author: taki
 */

#ifndef OPENMP_HELPER_H_
#define OPENMP_HELPER_H_

#include <omp.h>
unsigned int myseed = 0;
unsigned int myseed2 = 0;
unsigned int my_thread_id = 0;
unsigned int TOTAL_THREADS = 1;
#pragma omp threadprivate (myseed)
#pragma omp threadprivate (myseed2)
#pragma omp threadprivate (my_thread_id)


void init_omp_random_seeds(int initial_seed=0) {
	TOTAL_THREADS=1;
#pragma omp parallel
	{
		TOTAL_THREADS = omp_get_num_threads();
	}
	unsigned int seed[TOTAL_THREADS];
	unsigned int seed2[TOTAL_THREADS];
#pragma omp parallel
	{
		my_thread_id =omp_get_thread_num();
		if (omp_get_thread_num()==0) {
			srand(initial_seed);
			for (unsigned int i = 0; i < TOTAL_THREADS; i++){
			seed[i] = (unsigned int) RAND_MAX * rand();
			seed2[i] = (unsigned int) RAND_MAX * rand();
			}
		}
	}
#pragma omp parallel
	{
		myseed = seed[omp_get_thread_num()];
		myseed2 = seed2[omp_get_thread_num()];
	}
}


#endif /* OPENMP_HELPER_H_ */
