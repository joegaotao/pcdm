#include <RcppEigen.h>
#include <RcppGSL.h>
// [[Rcpp::depends(RcppEigen)]]
// [[Rcpp::depends(RcppGSL)]]
#include "structures.h"
#include "c_libs_headers.h"
#include "gsl_random_helper.h"
#include "losses.h"
#include "generator_nesterov.h"
using Eigen::MatrixXd;
using Eigen::VectorXd;
using Eigen::ArrayXd;
using Eigen::ArrayXXd;

using Rcpp::wrap;
using Rcpp::as;
using Rcpp::List;
using Rcpp::IntegerVector;
using Rcpp::NumericVector;
using Rcpp::NumericMatrix;
using Rcpp::Environment;
using Rcpp::Function;
using Rcpp::Named;

typedef Eigen::Map<MatrixXd> MapMat;
typedef Eigen::Map<VectorXd> MapVec;
typedef Eigen::Map<ArrayXd>  MapArray;
typedef Eigen::SparseVector<double> SpVec;
typedef Eigen::SparseMatrix<double> SpMat;
typedef std::vector<double> NormalVec;

template<typename L, typename D>
void run_computation(problem_data<L, D> &inst, std::vector<D> &h_Li,
    int omp_threads, L n, L m, int maxit, int blockReduction, std::vector<gsl_rng *>& rs,
		ofstream& experimentLogFile, const int MAXIMUM_THREADS) {
  
  omp_set_num_threads(omp_threads);
	init_random_seeds(rs);
	for (L i = 0; i < n; i++)
		inst.x[i] = 0;
	std::vector<D> residuals(m);
	Losses<L, D, square_loss_traits>::recompute_residuals(inst, residuals);
	D fvalInit = Losses<L, D, square_loss_traits>::compute_fast_objective(inst, residuals);
	double totalRunningTime = 0;
	double iterations = 0;
	L perPartIterations = n / blockReduction;
	double additional = perPartIterations / (0.0 + n);
	D fval = fvalInit;
// store initial objective value
	experimentLogFile << setprecision(16) << omp_threads << "," << n << "," << m
			<< "," << totalRunningTime << "," << iterations
			<< "," << fval << endl;
	//iterate
	for (int totalIt = 0; totalIt < maxit; totalIt++) {
		double startTime = gettime_();
#pragma omp parallel for
		for (L it = 0; it < perPartIterations; it++) {
			// one step of the algorithm
			unsigned long int idx = gsl_rng_uniform_int(gsl_rng_r, n);
      Losses<L, D, square_loss_traits>::do_single_iteration_parallel(
							inst, idx, residuals, inst.x, h_Li);
		}
		double endTime = gettime_();
		iterations += additional;
		totalRunningTime += endTime - startTime;
		// recompute residuals  - this step is not necessary but if accumulation of rounding errors occurs it is useful
		omp_set_num_threads(MAXIMUM_THREADS);
		if (totalIt % 3 == 0) {
			Losses<L, D, square_loss_traits>::recompute_residuals(inst, residuals);
		}
		fval = Losses<L, D, square_loss_traits>::compute_fast_objective(inst, residuals);
		int nnz = 0;
    double x_rmse = 0;
#pragma omp parallel for reduction(+:nnz)
		for (L i = 0; i < n; i++){
			if (inst.x[i] != 0)
				nnz++;
    }
// beta - x
//#pragma omp parallel for reduction(+:x_rmse)
//    for(int i = 0; i < n; i++){
//      x_rmse += (inst.x[i] - beta[i]) * (inst.x[i] - beta[i]);
//    }
    inst.x_rmse = sqrt(x_rmse)/n;
		omp_set_num_threads(omp_threads);
		cout << omp_threads << "," << n << ","
				<< m << "," << totalRunningTime << ","
				<< iterations << "," << fval << "," << nnz
				<< "," << inst.x_rmse << endl;

		experimentLogFile << setprecision(16) << omp_threads << "," << n << ","
				<< m << "," << totalRunningTime << ","
				<< iterations << "," << fval << "," << nnz
				<< ", " << inst.x_rmse << endl;
	}
}

void get_data(problem_data<int, double> &inst, SpMat &datX, NormalVec &datY, NormalVec &beta){
  int m = inst.m;
  int n = inst.n;
//#pragma omp parallel for schedule(dynamic)
  for (unsigned int i = 0; i < n; i++){
    for (unsigned int j = inst.A_csc_col_ptr[i]; j < inst.A_csc_col_ptr[i + 1]; j++){
      datX.insert(inst.A_csc_row_idx[j], i) = inst.A_csc_values[j];
    }
    beta[i] = inst.x[i];
  }
  datX.makeCompressed();
  for(int i = 0; i < m; i++){
    datY[i] = inst.b[i];
  }
}

// [[Rcpp::export]]
List pcdm_lasso(SEXP m_, SEXP n_, SEXP p_, SEXP maxit_, SEXP lambda_, SEXP weight_)
{
//BEGIN_RCPP
    problem_data<int, double> inst;
    // make the standard sparseMatrix format
    //SpMat datX(as<SpMat>(x_));
    /*
    MapMat XX(as<MapMat>(x_));
    SpMat datX = XX.sparseView();
    datX.makeCompressed();
    
    NormalVec datY(as<NormalVec>(y_));
    NormalVec beta(as<NormalVec>(beta_));
    int maxit(as<int>(maxit_));
    double lambda(as<double>(lambda_));
    int weight = int(as<bool>(weight_));
    // 初始化数据
    init_data(inst, datX, datY, lambda);
    */
    // 随机数初始化
    int m(as<int>(m_));
    int n(as<int>(n_));
    int p(as<int>(p_));
    int maxit(as<int>(maxit_));
    int lambda(as<int>(lambda_));
    int weight = int(as<bool>(weight_));

    SpMat datX(m, n);
    NormalVec beta(n);
    NormalVec datY(m);

    gsl_rng_env_setup();
    const gsl_rng_type * T;
  	gsl_rng * r;
  	T = gsl_rng_default;
  	r = gsl_rng_alloc(T);
    
    const int MAXIMUM_THREADS = 16;
	  std::vector<gsl_rng *> rs(MAXIMUM_THREADS);
	  for (int i = 0; i < MAXIMUM_THREADS; i++) {
		  rs[i] = gsl_rng_alloc(T);
		  gsl_rng_set(rs[i], i);
	  }
   init_omp_random_seeds();
   // 残差和v的初始化
   std::vector<double> h_Li(n, 0);
   //std::vector<double> residuals(m, 0);
   //---------------------- Set output files
   ofstream histogramLogFile;
   histogramLogFile.open("results/large_scale_expeiment_lasso_compare_histogram.log");
   ofstream experimentLogFile;
   experimentLogFile.open("results/large_scale_expeiment_lasso_compare.log");
   
  // run_experiment<long, double>(inst, n, m, maxit, 1, rs, histogramLogFile,
   //         experimentLogFile, MAXIMUM_THREADS);
   omp_set_num_threads(MAXIMUM_THREADS);
   init_random_seeds(rs);
	 double fvalOpt = nesterov_generator(inst, n, m, p, rs, histogramLogFile);
   get_data(inst, datX, datY, beta);
  //-------------- set the number of threads which should be used.
	 int TH[5] = {16, 8, 4, 2, 1 };
   for (int i = 0; i < 5; i++) {
     cout << "Running experiment with " << TH[i] << " threads" << endl;
     inst.sigma = 1 + (TH[i] - 1) * (inst.omega - 1) / (inst.n - 1);
     cout << setprecision(16) << "beta = " << inst.sigma << endl;
     if(weight < 1){// 求Lipschitz常数
       Losses<int, double, square_loss_traits>::compute_reciprocal_lipschitz_constants(inst, h_Li);
     }
     else{
       Losses<int, double, square_loss_traits>::compute_reciprocal_lipschitz_constants_weight(inst, h_Li, TH[i]);
     }
     // run the experiment
   	 run_computation<int, double>(inst, h_Li, TH[i], n, m, maxit, 1, rs, experimentLogFile, MAXIMUM_THREADS);
   }
   histogramLogFile.close();
   experimentLogFile.close(); 
   //Rcpp::S4 datX1(wrap(datX));
   //Rcpp::S4 datY1(wrap(datY));
   //Rcpp::S4 beta1(wrap(beta));
   
   return List::create(Named("datX") = datX,
                       Named("datY") = datY,
                       Named("beta") = beta,
                       Named("lambda") = lambda);
   

   //return List::create(Named("beta") = inst.x);
}



