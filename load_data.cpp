#include <RcppEigen.h>
  // [[Rcpp::depends(RcppEigen)]]
#include <structures.h>
using Eigen::MatrixXd;
using Eigen::VectorXd;

using Rcpp::wrap;
using Rcpp::as;
using Rcpp::List;
using Rcpp::Named;
using Rcpp::NumericVector;

typedef Eigen::Map<MatrixXd> MapMat;
typedef Eigen::Map<VectorXd> MapVec;
typedef Eigen::SparseVector<double> SpVec;
typedef Eigen::SparseMatrix<double> SpMat;
typedef std::vector<double> NormalVec;
typedef Eigen::MappedSparseMatrix< double > mappedSparseMatrix ;
// Below is a simple example of exporting a C++ function to R. You can
// source this function into an R session using the Rcpp::sourceCpp 
// function (or via the Source button on the editor toolbar)

// For more on using Rcpp click the Help button on the editor toolbar
// [[Rcpp::export]]
List load_data(SEXP x_, SEXP y_, SEXP lambda_)
{
  
  problem_data<int, double> inst;
  MapMat XX(as<MapMat>(x_));
  SpMat datX = XX.sparseView();
  datX.makeCompressed();
    
  NormalVec datY(as<NormalVec>(y_));
  double lambda(as<double>(lambda_));
    // 初始化数据
  int m = datX.rows();
  int n = datX.cols();
  inst.n = n;
  inst.m = m;
  inst.lambda = lambda;
  inst.b.resize(m, 0);
  inst.A_csc_values.resize(datX.nonZeros(), 0);
  inst.A_csc_col_ptr.resize(n + 1, 0);
  inst.A_csc_row_idx.resize(datX.nonZeros(), 0);
  int iteration = 0;
  for (int i = 0; i < datX.outerSize(); ++i){
    for (SpMat::InnerIterator it(datX, i); it; ++it){
      // 让inst.b得出每行有多少个非零元
      inst.b[it.row()]++; // inst.b[it.row()]++;
      //inst.A_csc_values.push_back[it.value()];
      inst.A_csc_values[iteration] = it.value();
      //inst.A_csc_row_idx.push_back[it.row()];
      inst.A_csc_row_idx[iteration] = it.row();
      iteration++;
    }
    inst.A_csc_col_ptr[i + 1] += iteration;
  }
  for(int i = 0; i < m; i++){
    inst.omega_ind.push_back(inst.b[i]);
  }
  // 获取每行非零元个数的最大值
  int min = n;
  double max = 0;
  for (long i = 0; i < m; i++) {
    if (inst.b[i] > max)
      max = inst.b[i];
    if (inst.b[i] < min)
      min = inst.b[i];
  }
  inst.omega = max;
  for(int i = 0; i < m; i++){
    inst.b[i] = datY[i];
  }
  inst.total_n = datX.nonZeros();
  inst.x.resize(n, 0);
  inst.x_rmse = 0; 
  
  return List::create(Named("x") = inst.A_csc_values,
      Named("col") = inst.n,
      Named("row") = inst.m,
      Named("x_col_ptr") = inst.A_csc_col_ptr,
      Named("x_row_idx") = inst.A_csc_row_idx,
      Named("y") = inst.b,
      Named("lambda") = inst.lambda,
      Named("total_nnz") = inst.total_n,
      Named("omega") = inst.omega,
      Named("omega_ind") = inst.omega_ind,
      Named("beta") = inst.x,
      Named("beta_rmse") = inst.x_rmse
      );
}
